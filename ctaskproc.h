#ifndef CTASKPROC_H
#define CTASKPROC_H

#include <QObject>
#include <QtNetwork>


class CTaskProc : public QObject
{
    Q_OBJECT
public:
    explicit CTaskProc(int id,QTcpSocket* &client, QString ip, int port, QObject *parent = 0);
    ~CTaskProc();

    int getId(){return m_id;}
    int isValid(){return m_valid;}
    void stop();
    void sendToServer(QByteArray &data); //发给svr的数据
    void sendToDevice(QByteArray &data); //发给dev的数据

signals:
    void sigTaskEnd(int id);
    void sigReadLocalClientData(int, QByteArray &);
    void sigReadSvrClientData(int, QByteArray &);
    void sigTaskStartOk(int id);

public slots:

    void recvClientData();
    void recvSvrData();
    void diconnect();
    void procConnectErr(QAbstractSocket::SocketError);
    void procConnectOk();

private:
    int m_id;
    QString m_ip;
    int m_port;

    bool m_valid;
    QTcpSocket *pLocalClient;
    QTcpSocket *pToSvrClient;
};

#endif // CTASKPROC_H
